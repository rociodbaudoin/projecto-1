package modulo;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class pantallaro {

	private JFrame frame;
	private JLabel lblTiposDeDatos;
	private JLabel lblMinimo;
	private JLabel lblMaximo;
	private JLabel lblByte;
	private JLabel lblminimobyte;
	private JLabel lblmaximobyte;
	private JLabel lblShort;
	private JLabel lblminimoshort;
	private JLabel lblmaximoshort;
	private JLabel lblInt;
	private JLabel lblminimoint;
	private JLabel lblmaximoint;
	private JLabel lblLong;
	private JLabel lblminimolong;
	private JLabel lblmaximolong;
	private JButton btnCalcular;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pantallaro window = new pantallaro();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public pantallaro() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.GRAY);
		frame.getContentPane().setForeground(Color.BLACK);
		frame.setBounds(100, 100, 474, 327);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		lblTiposDeDatos = new JLabel("Tipos de Datos ");
		lblTiposDeDatos.setForeground(Color.BLACK);
		lblTiposDeDatos.setFont(new Font("Calibri", Font.BOLD, 14));
		lblTiposDeDatos.setBounds(46, 54, 102, 23);
		frame.getContentPane().add(lblTiposDeDatos);

		lblMinimo = new JLabel("Minimo");
		lblMinimo.setHorizontalTextPosition(SwingConstants.CENTER);
		lblMinimo.setHorizontalAlignment(SwingConstants.CENTER);
		lblMinimo.setForeground(Color.BLACK);
		lblMinimo.setFont(new Font("Calibri", Font.BOLD, 14));
		lblMinimo.setBounds(187, 58, 58, 14);
		frame.getContentPane().add(lblMinimo);

		lblMaximo = new JLabel("Maximo");
		lblMaximo.setHorizontalTextPosition(SwingConstants.CENTER);
		lblMaximo.setHorizontalAlignment(SwingConstants.CENTER);
		lblMaximo.setForeground(Color.BLACK);
		lblMaximo.setFont(new Font("Calibri", Font.BOLD, 14));
		lblMaximo.setBounds(329, 58, 76, 14);
		frame.getContentPane().add(lblMaximo);

		lblByte = new JLabel("Byte");
		lblByte.setHorizontalAlignment(SwingConstants.CENTER);
		lblByte.setForeground(Color.BLACK);
		lblByte.setFont(new Font("Calibri", Font.BOLD, 14));
		lblByte.setBounds(46, 83, 46, 14);
		frame.getContentPane().add(lblByte);

		lblminimobyte = new JLabel("");
		lblminimobyte.setForeground(Color.BLACK);
		lblminimobyte.setHorizontalAlignment(SwingConstants.CENTER);
		lblminimobyte.setHorizontalTextPosition(SwingConstants.CENTER);
		lblminimobyte.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblminimobyte.setFont(new Font("Cambria", Font.BOLD, 11));
		lblminimobyte.setBackground(Color.GRAY);
		lblminimobyte.setOpaque(true);
		lblminimobyte.setBounds(144, 83, 134, 14);
		frame.getContentPane().add(lblminimobyte);

		lblmaximobyte = new JLabel("");
		lblmaximobyte.setForeground(Color.BLACK);
		lblmaximobyte.setHorizontalTextPosition(SwingConstants.CENTER);
		lblmaximobyte.setHorizontalAlignment(SwingConstants.CENTER);
		lblmaximobyte.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblmaximobyte.setFont(new Font("Cambria", Font.BOLD, 11));
		lblmaximobyte.setOpaque(true);
		lblmaximobyte.setBackground(Color.GRAY);
		lblmaximobyte.setBounds(308, 83, 115, 14);
		frame.getContentPane().add(lblmaximobyte);

		lblShort = new JLabel("Short");
		lblShort.setHorizontalAlignment(SwingConstants.CENTER);
		lblShort.setForeground(Color.BLACK);
		lblShort.setFont(new Font("Calibri", Font.BOLD, 14));
		lblShort.setBounds(46, 113, 46, 14);
		frame.getContentPane().add(lblShort);

		lblInt = new JLabel("Int");
		lblInt.setHorizontalAlignment(SwingConstants.CENTER);
		lblInt.setForeground(Color.BLACK);
		lblInt.setFont(new Font("Calibri", Font.BOLD, 14));
		lblInt.setBounds(46, 138, 46, 14);
		frame.getContentPane().add(lblInt);

		lblLong = new JLabel("Long");
		lblLong.setHorizontalAlignment(SwingConstants.CENTER);
		lblLong.setForeground(Color.BLACK);
		lblLong.setFont(new Font("Calibri", Font.BOLD, 14));
		lblLong.setBounds(46, 163, 46, 14);
		frame.getContentPane().add(lblLong);

		lblminimoshort = new JLabel("");
		lblminimoshort.setForeground(Color.BLACK);
		lblminimoshort.setHorizontalTextPosition(SwingConstants.CENTER);
		lblminimoshort.setHorizontalAlignment(SwingConstants.CENTER);
		lblminimoshort.setFont(new Font("Cambria", Font.BOLD, 11));
		lblminimoshort.setBackground(Color.GRAY);
		lblminimoshort.setOpaque(true);
		lblminimoshort.setBounds(144, 108, 134, 14);
		frame.getContentPane().add(lblminimoshort);

		lblmaximoshort = new JLabel("");
		lblmaximoshort.setForeground(Color.BLACK);
		lblmaximoshort.setHorizontalTextPosition(SwingConstants.CENTER);
		lblmaximoshort.setHorizontalAlignment(SwingConstants.CENTER);
		lblmaximoshort.setFont(new Font("Cambria", Font.BOLD, 11));
		lblmaximoshort.setOpaque(true);
		lblmaximoshort.setBackground(Color.GRAY);
		lblmaximoshort.setBounds(308, 108, 116, 14);
		frame.getContentPane().add(lblmaximoshort);

		lblminimoint = new JLabel("");
		lblminimoint.setHorizontalTextPosition(SwingConstants.CENTER);
		lblminimoint.setHorizontalAlignment(SwingConstants.CENTER);
		lblminimoint.setFont(new Font("Cambria", Font.BOLD, 11));
		lblminimoint.setForeground(Color.BLACK);
		lblminimoint.setBackground(Color.GRAY);
		lblminimoint.setOpaque(true);
		lblminimoint.setBounds(144, 133, 134, 14);
		frame.getContentPane().add(lblminimoint);

		lblmaximoint = new JLabel("");
		lblmaximoint.setForeground(Color.BLACK);
		lblmaximoint.setHorizontalTextPosition(SwingConstants.CENTER);
		lblmaximoint.setHorizontalAlignment(SwingConstants.CENTER);
		lblmaximoint.setFont(new Font("Cambria", Font.BOLD, 11));
		lblmaximoint.setOpaque(true);
		lblmaximoint.setBackground(Color.GRAY);
		lblmaximoint.setBounds(308, 134, 116, 14);
		frame.getContentPane().add(lblmaximoint);

		lblminimolong = new JLabel("");
		lblminimolong.setForeground(Color.BLACK);
		lblminimolong.setHorizontalTextPosition(SwingConstants.CENTER);
		lblminimolong.setHorizontalAlignment(SwingConstants.CENTER);
		lblminimolong.setFont(new Font("Cambria", Font.BOLD, 11));
		lblminimolong.setBackground(Color.GRAY);
		lblminimolong.setOpaque(true);
		lblminimolong.setBounds(108, 163, 161, 14);
		frame.getContentPane().add(lblminimolong);

		lblmaximolong = new JLabel("");
		lblmaximolong.setForeground(Color.BLACK);
		lblmaximolong.setHorizontalTextPosition(SwingConstants.CENTER);
		lblmaximolong.setHorizontalAlignment(SwingConstants.CENTER);
		lblmaximolong.setFont(new Font("Cambria", Font.BOLD, 11));
		lblmaximolong.setOpaque(true);
		lblmaximolong.setBackground(Color.GRAY);
		lblmaximolong.setBounds(273, 163, 161, 14);
		frame.getContentPane().add(lblmaximolong);

		btnCalcular = new JButton("Calcular");
		btnCalcular.setFont(new Font("Calibri", Font.BOLD, 14));
		btnCalcular.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnCalcular.setHideActionText(true);
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblminimobyte.setText("-128");
				lblmaximobyte.setText("128");
				lblminimoshort.setText("-32.767");
				lblmaximoshort.setText("32.767");
				lblminimoint.setText("-2.147.483.648");
				lblmaximoint.setText("2.147.483.648");
				lblminimolong.setText("-9.223.372.036.854.775.808");
				lblmaximolong.setText("9.223.372.036.854.775.808");

			}
		});
		btnCalcular.setBounds(329, 242, 89, 23);
		frame.getContentPane().add(btnCalcular);

		label = new JLabel("");
		label.setIcon(new ImageIcon(pantallaro.class
				.getResource("/iconos/rayo_32px.png")));
		label.setBounds(10, 11, 29, 48);
		frame.getContentPane().add(label);

		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblminimobyte.setText("");
				lblmaximobyte.setText("");
				lblminimoshort.setText("");
				lblmaximoshort.setText("");
				lblminimoint.setText("");
				lblmaximoint.setText("");
				lblminimolong.setText("");
				lblmaximolong.setText("");
				
			}
		});
		btnBorrar.setFont(new Font("Calibri", Font.BOLD, 14));
		btnBorrar.setBounds(187, 242, 89, 23);
		frame.getContentPane().add(btnBorrar);

	}
}
