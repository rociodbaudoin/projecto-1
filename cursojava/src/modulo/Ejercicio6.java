package modulo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio6 {

	private JFrame frame;
	private JTextField txtEdad;
	private int iedad;
	private JLabel lblNivel;
	private JButton btnIngresar;
	private JButton btnBorrar;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio6 window = new Ejercicio6();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio6() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblAlumnos = new JLabel("Alumnos ");
		lblAlumnos.setFont(new Font("Dialog", Font.PLAIN, 19));
		lblAlumnos.setBounds(166, 11, 79, 25);
		frame.getContentPane().add(lblAlumnos);
		
		JLabel lblEdadDelAlumno = new JLabel("Edad del Alumno ");
		lblEdadDelAlumno.setFont(new Font("Dialog", Font.PLAIN, 15));
		lblEdadDelAlumno.setBounds(24, 57, 114, 20);
		frame.getContentPane().add(lblEdadDelAlumno);
		
		JLabel lblNivelAlQue = new JLabel("Nivel al que pertenese ");
		lblNivelAlQue.setFont(new Font("Dialog", Font.PLAIN, 15));
		lblNivelAlQue.setBounds(10, 104, 147, 20);
		frame.getContentPane().add(lblNivelAlQue);
		
		txtEdad = new JTextField();
		txtEdad.setDisabledTextColor(Color.GRAY);
		txtEdad.setBounds(166, 59, 86, 20);
		frame.getContentPane().add(txtEdad);
		txtEdad.setColumns(10);
		
		lblNivel = new JLabel("");
		lblNivel.setFont(new Font("Dialog", Font.PLAIN, 15));
		lblNivel.setOpaque(true);
		lblNivel.setBackground(Color.WHITE);
		lblNivel.setBounds(161, 99, 144, 25);
		frame.getContentPane().add(lblNivel);
		
		btnIngresar = new JButton("Ingresar");
		btnIngresar.setFont(new Font("Dialog", Font.PLAIN, 11));
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int iedad = Integer.parseInt(txtEdad.getText());
						
				if(iedad<=5){
					lblNivel.setText("Nivel Inicial");
				}
				
					
				else if (iedad>=6) {
					
					lblNivel.setText("Nivel Primario");
				}
				if (iedad>=12){
					lblNivel.setText("Nivel Secundario");
				}
				
				}
		});
		
		
		btnIngresar.setBounds(216, 202, 89, 23);
		frame.getContentPane().add(btnIngresar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setFont(new Font("Dialog", Font.PLAIN, 11));
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtEdad .setText("");
				lblNivel.setText("");
			}
			
		});
		btnBorrar.setBounds(73, 202, 89, 23);
		frame.getContentPane().add(btnBorrar);
	}
}
	

