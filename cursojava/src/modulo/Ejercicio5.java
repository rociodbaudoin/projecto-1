package modulo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio5 {

	private JFrame frame;
	private JTextField txtPuntos;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio5 window = new Ejercicio5();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio5() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblEjercicio = new JLabel("Ejercicio 5");
		lblEjercicio.setFont(new Font("Dialog", Font.PLAIN, 24));
		lblEjercicio.setBounds(159, 11, 118, 33);
		frame.getContentPane().add(lblEjercicio);
		
		JLabel lblPuntosObtenidos = new JLabel("Puntos obtenidos:");
		lblPuntosObtenidos.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblPuntosObtenidos.setBounds(10, 65, 108, 16);
		frame.getContentPane().add(lblPuntosObtenidos);
		
		txtPuntos = new JTextField();
		txtPuntos.setFont(new Font("Dialog", Font.PLAIN, 15));
		txtPuntos.setBounds(151, 64, 86, 20);
		frame.getContentPane().add(txtPuntos);
		txtPuntos.setColumns(10);
		
		JLabel lblResultado = new JLabel("");
		lblResultado.setBackground(Color.GRAY);
		lblResultado.setFont(new Font("Dialog", Font.PLAIN, 17));
		lblResultado.setBounds(37, 125, 179, 33);
		frame.getContentPane().add(lblResultado);
		
		JLabel lblIcono = new JLabel("");
		lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("")));
		lblIcono.setOpaque(true);
		lblIcono.setFont(new Font("Dialog", Font.PLAIN, 16));
		lblIcono.setBounds(262, 109, 57, 56);
		frame.getContentPane().add(lblIcono);
		
		JButton btnResultado = new JButton("Resultado");
		btnResultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int ipunt = Integer.parseInt(txtPuntos.getText());
									
					 if(ipunt<50){
						lblResultado.setText("3er Lugar");
						lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("/iconos/medalla_bronce_64px.png")));
					}
					
					 	if(ipunt>=50){
							lblResultado.setText("2do Lugar");
							lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("/iconos/medalla_plata_64px.png")));
						
					}
					 		if (ipunt>=80){
								lblResultado.setText("1er Lugar");
								lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("/iconos/medalla_oro_64px.png")));
					}
					 			if (ipunt<20){
					 					lblResultado.setText("Segui Participando");
					 					lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("/iconos/fantasma_32px.png")));}
				}
			
		});
		btnResultado.setBounds(279, 206, 102, 23);
		frame.getContentPane().add(btnResultado);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblResultado.setText("");
				lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("")));
				txtPuntos.setText("");
			}
		});
		btnBorrar.setBounds(73, 206, 89, 23);
		frame.getContentPane().add(btnBorrar);
	}

}
