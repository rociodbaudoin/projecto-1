package modulo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio4 {

	private JFrame frame;
	private JTextField txtEdad;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio4 window = new Ejercicio4();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio4() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ejercicio 4");
		lblNewLabel.setFont(new Font("Dialog", Font.PLAIN, 24));
		lblNewLabel.setBounds(161, 11, 110, 32);
		frame.getContentPane().add(lblNewLabel);
		
		txtEdad = new JTextField();
		txtEdad.setBounds(136, 54, 86, 20);
		frame.getContentPane().add(txtEdad);
		txtEdad.setColumns(10);
		
		JLabel lblIngreseEdad = new JLabel("Ingrese Edad:");
		lblIngreseEdad.setFont(new Font("Dialog", Font.PLAIN, 16));
		lblIngreseEdad.setBounds(25, 51, 97, 21);
		frame.getContentPane().add(lblIngreseEdad);
		
		JLabel lblCategoria = new JLabel("");
		lblCategoria.setBackground(Color.GRAY);
		lblCategoria.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblCategoria.setOpaque(true);
		lblCategoria.setBounds(136, 140, 174, 32);
		frame.getContentPane().add(lblCategoria);
		
		JLabel lblCategoria_1 = new JLabel("Categoria:");
		lblCategoria_1.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblCategoria_1.setBounds(25, 151, 66, 19);
		frame.getContentPane().add(lblCategoria_1);
		
		JButton btnVer = new JButton("Ver");
		btnVer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int iedad = Integer.parseInt(txtEdad.getText());
				if (iedad<=25){
					lblCategoria.setText("Hijos");
				}
					if (iedad<50){
						lblCategoria.setText("Padres");
					}
					else if (iedad>=51){
						lblCategoria.setText("Abuelos");
					}
			}
		});
		btnVer.setFont(new Font("Dialog", Font.PLAIN, 15));
		btnVer.setBounds(292, 208, 89, 23);
		frame.getContentPane().add(btnVer);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblCategoria.setText("");
				txtEdad.setText("");
			}
		});
		btnBorrar.setFont(new Font("Dialog", Font.PLAIN, 14));
		btnBorrar.setBounds(70, 210, 89, 23);
		frame.getContentPane().add(btnBorrar);
	}
}
