package modulo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio3_ {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio3_ window = new Ejercicio3_();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio3_() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Calendario");
		lblNewLabel.setFont(new Font("Dialog", Font.PLAIN, 25));
		lblNewLabel.setBounds(142, 11, 122, 33);
		frame.getContentPane().add(lblNewLabel);
		
		JComboBox cmbMeses = new JComboBox();
		cmbMeses.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		cmbMeses.setBounds(91, 66, 81, 20);
		frame.getContentPane().add(cmbMeses);
		
		JLabel lblNewLabel_1 = new JLabel("Meses");
		lblNewLabel_1.setFont(new Font("Dialog", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(10, 69, 46, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblDias = new JLabel("");
		lblDias.setBackground(Color.WHITE);
		lblDias.setOpaque(true);
		lblDias.setFont(new Font("Dialog", Font.PLAIN, 19));
		lblDias.setBounds(0, 135, 424, 33);
		frame.getContentPane().add(lblDias);
		
		JButton btnVer = new JButton("Ver");
		btnVer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int imes = Integer.parseInt((String) cmbMeses.getSelectedItem());
				switch (imes) {
				  case 1: case 3: case 5: case 7: case 8: case 10: case 12:
				    lblDias.setText("El mes tiene 31 d�as");
				    break;
				  case 4: case 6: case 9: case 11:
					  lblDias.setText("El mes tiene 30 d�as");
				    break;
				  case 2:			
					  lblDias.setText("El mes tiene 28 d�as (o 29 d�as si es a�o bisiesto)");
				    break;			
				  default:
				    lblDias.setText("Mes incorrecto");
				}
			}
		});
		btnVer.setBounds(277, 197, 89, 23);
		frame.getContentPane().add(btnVer);
	}

}
