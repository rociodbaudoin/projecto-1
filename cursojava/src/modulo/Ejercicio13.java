package modulo;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Ejercicio13 {

	private JFrame frame;
	private JLabel lblNewLabel;
	private JComboBox cmbMeses;
	private JLabel lblNewLabel_1;
	private JLabel lblDias;
	private JButton btnVer;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio13 window = new Ejercicio13();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio13() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		lblNewLabel = new JLabel("Ejercicio 13");
		lblNewLabel.setFont(new Font("Dialog", Font.PLAIN, 25));
		lblNewLabel.setBounds(142, 11, 132, 33);
		frame.getContentPane().add(lblNewLabel);
		
		cmbMeses = new JComboBox();
		cmbMeses.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"}));
		cmbMeses.setBounds(91, 66, 81, 20);
		frame.getContentPane().add(cmbMeses);
		
		lblNewLabel_1 = new JLabel("Meses");
		lblNewLabel_1.setFont(new Font("Dialog", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(10, 69, 46, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		lblDias = new JLabel("");
		lblDias.setBackground(Color.WHITE);
		lblDias.setOpaque(true);
		lblDias.setFont(new Font("Dialog", Font.PLAIN, 19));
		lblDias.setBounds(0, 135, 424, 33);
		frame.getContentPane().add(lblDias);
		
		btnVer = new JButton("Ver");
		btnVer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int imes = Integer.parseInt((String) cmbMeses.getSelectedItem());
				switch (imes) {
				  case 1: case 3: case 5: case 7: case 8: case 10: case 12:
				    lblDias.setText("El mes tiene 31 d�as");
				    break;
				  case 4: case 6: case 9: case 11:
					  lblDias.setText("El mes tiene 30 d�as");
				    break;
				  case 2:			
					  lblDias.setText("El mes tiene 28 d�as (o 29 d�as si es a�o bisiesto)");
				    break;			
				  default:
				    lblDias.setText("Mes incorrecto");
				}
			}
	
			
		});
		btnVer.setBounds(288, 205, 89, 23);
		frame.getContentPane().add(btnVer);
		
		
	}



	}


