package modulo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.List;

import javax.swing.JTextField;
import javax.swing.AbstractButton;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;

public class Ejercicio9 {


	private JFrame frame;
	private JLabel lblSeleccionDePc;
	private JLabel lblSelecPc;
	private JLabel lblElijaUna;
	private JLabel lblResultado;
	private JLabel lblGano;
	private JButton btnJugar;
	private JComboBox cmbElija;
	private String inum;
	private int iselecpc;
	private JLabel label;
	static final int piedra = 1;
	static final int papel = 2;
	static final int tijeras = 3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio9 window = new Ejercicio9();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio9() {
		initialize();}
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Dialog", Font.PLAIN, 11));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPiedraPapelO = new JLabel("Piedra, Papel o Tijera");
		lblPiedraPapelO.setFont(new Font("Dialog", Font.PLAIN, 18));
		lblPiedraPapelO.setBounds(128, 11, 174, 24);
		frame.getContentPane().add(lblPiedraPapelO);
		
		lblSeleccionDePc = new JLabel("seleccion de pc:");
		lblSeleccionDePc.setBounds(25, 103, 76, 14);
		frame.getContentPane().add(lblSeleccionDePc);
		
		lblResultado = new JLabel("Resultado: ");
		lblResultado.setBounds(25, 165, 85, 14);
		frame.getContentPane().add(lblResultado);
		
		lblElijaUna = new JLabel("Elija una:");
		lblElijaUna.setBounds(46, 67, 46, 14);
		frame.getContentPane().add(lblElijaUna);
		
		cmbElija = new JComboBox();
		cmbElija.addItem("Piedra");
		cmbElija.addItem("Papel");
		cmbElija.addItem("Tijeras");
		cmbElija.setEditable(true);
		cmbElija.setBounds(127, 64, 98, 20);
		frame.getContentPane().add(cmbElija);		
	       
		
		
		label = new JLabel("");
		label.setIcon(new ImageIcon(Ejercicio9.class.getResource("")));
		label.setBounds(308, 118, 68, 61);
		frame.getContentPane().add(label);
		
		lblGano = new JLabel("");
		lblGano.setFont(new Font("Dialog", Font.PLAIN, 11));
		lblGano.setOpaque(true);
		lblGano.setBackground(Color.WHITE);
		lblGano.setBounds(121, 149, 140, 30);
		frame.getContentPane().add(lblGano);
		
		lblSelecPc = new JLabel("");
		lblSelecPc.setVisible(true);
		lblSelecPc.setBackground(Color.GRAY);
		lblSelecPc.setOpaque(true);
		iselecpc = (int) (Math.random()*3);
		lblSelecPc.setText(Integer.toString(iselecpc));
		lblSelecPc.setBounds(121, 103, 76, 14);
		frame.getContentPane().add(lblSelecPc);
		
	 
		
		btnJugar = new JButton("Jugar");
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inum=String.valueOf(cmbElija.getSelectedItem());
				iselecpc =Integer.parseInt((String)lblSelecPc.getText());	 
				
				
				if (inum=="Piedra"&& iselecpc==0){
					 lblGano.setText("Empate");
					 label.setIcon(new ImageIcon(Ejercicio9.class.getResource("/iconos/dedoArriba_32px.png")));}
					if (inum=="Papel"&& iselecpc==1){
					 	lblGano.setText("Empate");
					 	label.setIcon(new ImageIcon(Ejercicio9.class.getResource("/iconos/dedoArriba_32px.png")));}
						if (inum=="Tijeras"&& iselecpc==2){
								lblGano.setText("Empate");
								label.setIcon(new ImageIcon(Ejercicio9.class.getResource("/iconos/dedoArriba_32px.png")));}
				      //papel cubre piedra...
				 if (inum=="Papel" && iselecpc==0){
				    	   lblGano.setText("Ganaste");
				    	   label.setIcon(new ImageIcon(Ejercicio9.class.getResource("/iconos/Gano_32px.png")));
				       }
				      //tijeras cortan papel...
					 	 if (inum=="Tijeras" && iselecpc==1){
								lblGano.setText("Ganaste");
								label.setIcon(new ImageIcon(Ejercicio9.class.getResource("/iconos/Gano_32px.png")));
				    	   }
							//piedra rompe tijeras
								 if (inum=="Piedra" && iselecpc==2){
											lblGano.setText("Ganaste");
											label.setIcon(new ImageIcon(Ejercicio9.class.getResource("/iconos/Gano_32px.png")));
						       }
								  
								 else if (inum=="Tijeras" && iselecpc==0){
										lblGano.setText("perdiste");
										label.setIcon(new ImageIcon(Ejercicio9.class.getResource("/iconos/dedoAbajo_32px.png")));
						       }
								 if (inum=="Papel" && iselecpc==2){
										lblGano.setText("perdiste");
										label.setIcon(new ImageIcon(Ejercicio9.class.getResource("/iconos/dedoAbajo_32px.png")));
						       }
								 if (inum=="Piedra" && iselecpc==1){
										lblGano.setText("perdiste");
										label.setIcon(new ImageIcon(Ejercicio9.class.getResource("/iconos/dedoAbajo_32px.png")));
						       }
								 
					}
			
			
			
		});
		
		btnJugar.setBounds(287, 214, 89, 23);
		frame.getContentPane().add(btnJugar);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iselecpc = (int) (Math.random()*3);
				lblSelecPc.setText(Integer.toString(iselecpc));
				lblGano.setText("");
				label.setIcon(new ImageIcon(Ejercicio9.class.getResource("")));
				
			}
		});
		btnReset.setBounds(106, 214, 89, 23);
		frame.getContentPane().add(btnReset);
	
	}
	}

		
		
		
		
		
		
	

		


