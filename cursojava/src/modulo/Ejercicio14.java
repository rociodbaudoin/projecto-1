package modulo;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Ejercicio14 {

	private JFrame frame;
	private JTextField txtPuntos;
	private JLabel lblResultado;
	private JLabel lblIcono;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio14 window = new Ejercicio14();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio14() {
		initialize();}
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
	
		JLabel lblEjercicio = new JLabel("Ejercicio 5");
		lblEjercicio.setFont(new Font("Dialog", Font.PLAIN, 24));
		lblEjercicio.setBounds(159, 11, 118, 33);
		frame.getContentPane().add(lblEjercicio);
		
		JLabel lblPuntosObtenidos = new JLabel("Puntos obtenidos:");
		lblPuntosObtenidos.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblPuntosObtenidos.setBounds(10, 65, 108, 16);
		frame.getContentPane().add(lblPuntosObtenidos);
		
		txtPuntos = new JTextField();
		txtPuntos.setFont(new Font("Dialog", Font.PLAIN, 15));
		txtPuntos.setBounds(151, 64, 86, 20);
		frame.getContentPane().add(txtPuntos);
		txtPuntos.setColumns(10);
		
		
		lblResultado = new JLabel("");
		lblResultado.setBackground(Color.GRAY);
		lblResultado.setFont(new Font("Dialog", Font.PLAIN, 17));
		lblResultado.setBounds(37, 125, 179, 33);
		frame.getContentPane().add(lblResultado);
		
		lblIcono = new JLabel("");
		lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("")));
		lblIcono.setOpaque(true);
		lblIcono.setFont(new Font("Dialog", Font.PLAIN, 16));
		lblIcono.setBounds(262, 109, 57, 56);
		frame.getContentPane().add(lblIcono);
		
		JButton btnResultado = new JButton("Resultado");
		btnResultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int ipunt = Integer.parseInt(txtPuntos.getText());
				
				
				if(ipunt >= 0){
					lblResultado.setText("Ingrese un numero valido");
					lblResultado.setFont(new Font("Tahoma", Font.PLAIN, 15));
					lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("/iconos/fallido_32px.png")));}
					
					
				
				switch (ipunt) {
				case 80: 
					lblResultado.setText("Primer Lugar");
					lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("/iconos/medalla_oro_64px.png")));
					break;
				case 50: 
					lblResultado.setText("Segundo Lugar");
					lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("/iconos/medalla_plata_64px.png")));
					break;
				case 30: 
					lblResultado.setText("Tercer Lugar");
					lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("/iconos/medalla_bronce_64px.png")));
					break;}
					
				
				
			}
			
	
			
				
		});
		btnResultado.setBounds(279, 206, 102, 23);
		frame.getContentPane().add(btnResultado);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblResultado.setText("");
				lblIcono.setIcon(new ImageIcon(Ejercicio5.class.getResource("")));
				txtPuntos.setText("");
			
			}
		});
		btnBorrar.setBounds(97, 206, 89, 23);
		frame.getContentPane().add(btnBorrar);
		

	
	
		}


	};
	

