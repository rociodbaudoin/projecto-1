package modulo;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class GuessANumber {

	private JFrame frame;
	private JLabel lblicon;
	private JLabel lblTextoArribaAbajo;
	private JLabel lblElegido;
	private JButton btnAceptar;
	private  int cantidadIntentos_1;//va a contar los intentos, es un contador
	private JButton btnLimpiar;
	private JLabel lblNumeroElejido;
	private JLabel lblCantidadIntentos_1;
	private JComboBox cmbNumeroElegido;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuessANumber window = new GuessANumber();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GuessANumber() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblElijeUnNumero = new JLabel("Elije un numero");
		lblElijeUnNumero.setFont(new Font("Dialog", Font.PLAIN, 18));
		lblElijeUnNumero.setBounds(164, 11, 124, 22);
		frame.getContentPane().add(lblElijeUnNumero);
		
		JLabel lblElijeUnNumero_1 = new JLabel("elije un numero entre 1 y 99");
		lblElijeUnNumero_1.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblElijeUnNumero_1.setBounds(28, 98, 177, 19);
		frame.getContentPane().add(lblElijeUnNumero_1);
		
		JLabel lblCantidadDeIntentos = new JLabel("Cantidad de intentos");
		lblCantidadDeIntentos.setFont(new Font("Dialog", Font.PLAIN, 13));
		lblCantidadDeIntentos.setBounds(34, 60, 119, 18);
		frame.getContentPane().add(lblCantidadDeIntentos);
		
		lblicon = new JLabel("");
		lblicon.setIcon(new ImageIcon(GuessANumber.class.getResource("")));
		lblicon.setOpaque(true);
		lblicon.setBounds(350, 85, 53, 55);
		frame.getContentPane().add(lblicon);
		
		lblTextoArribaAbajo = new JLabel("mas alto");
		lblTextoArribaAbajo.setHorizontalTextPosition(SwingConstants.CENTER);
		lblTextoArribaAbajo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTextoArribaAbajo.setVisible(false);
		lblTextoArribaAbajo.setBackground(Color.WHITE);
		lblTextoArribaAbajo.setOpaque(true);
		lblTextoArribaAbajo.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblTextoArribaAbajo.setBounds(315, 60, 100, 19);
		frame.getContentPane().add(lblTextoArribaAbajo);
		
		lblCantidadIntentos_1 = new JLabel("");
		lblCantidadIntentos_1.setBackground(Color.WHITE);
		lblCantidadIntentos_1.setBounds(67, 85, 46, 14);
		frame.getContentPane().add(lblCantidadIntentos_1);
		
		
		
		
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			

			public void actionPerformed(ActionEvent e) {
			int imiValor = Integer.parseInt((String)cmbNumeroElegido.getSelectedItem());
			int ivalorCompu = Integer.parseInt(lblElegido.getText());
			if(imiValor>ivalorCompu){
				cantidadIntentos_1++;
				lblTextoArribaAbajo.setText("mas bajo");
				
				lblTextoArribaAbajo.setVisible(true);
				lblicon.setVisible(true);
				lblicon.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/fallido_32px.png")));
				
				
			} else if (imiValor<ivalorCompu){ 
				cantidadIntentos_1++;
				lblTextoArribaAbajo.setText("mas alto");
				
				lblTextoArribaAbajo.setVisible(true);
				lblicon.setVisible(true);
				lblicon.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/fallido2_32px.png")));
				
			} 
			 else{
				cantidadIntentos_1++;
				lblTextoArribaAbajo.setText("BIEN GENIO!");
				
				lblTextoArribaAbajo.setVisible(true);
				lblElegido.setVisible(true);
				lblicon.setVisible(true);
				lblicon.setIcon(new ImageIcon(GuessANumber.class.getResource("/iconos/correcto_32px.png")));
			} 
			lblCantidadIntentos_1.setText(Integer.toString(cantidadIntentos_1));
			}
		});
		btnAceptar.setActionCommand("Aceptar");
		btnAceptar.setBounds(221, 227, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		lblElegido = new JLabel("");
		lblElegido.setForeground(Color.GREEN);
		lblElegido.setVisible(false);
		lblElegido.setFont(new Font("Dialog", Font.BOLD, 20));
		lblElegido.setBackground(Color.WHITE);
		lblElegido.setOpaque(true);
		lblElegido.setBounds(225, 136, 46, 47);
		int iElegido = (int) (Math.random()*1000%100);
		lblElegido.setText(Integer.toString(iElegido));
		frame.getContentPane().add(lblElegido);
		
		
		
		
		
		
		
		
	
		
		lblNumeroElejido = new JLabel("Numero Elejido");
		lblNumeroElejido.setFont(new Font("Dialog", Font.PLAIN, 13));
		lblNumeroElejido.setBounds(105, 140, 88, 18);
		frame.getContentPane().add(lblNumeroElejido);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblTextoArribaAbajo.setText("");
				lblicon.setVisible(false);
				cmbNumeroElegido.setModel(new DefaultComboBoxModel());
				String strList[]= new String[99];
				for (int i=0;i<99;i++)
					strList [i]= Integer.toString(i+1);
				cmbNumeroElegido.setModel(new DefaultComboBoxModel(strList));
				lblElegido.setVisible(false);
				int iElegido = (int) (Math.random()*1000%100);
				lblElegido.setText(Integer.toString(iElegido));
				int ivalorCompu = Integer.parseInt(lblElegido.getText());
				
							
				

			
			}
		});
		
		btnLimpiar.setBounds(326, 227, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		
		cmbNumeroElegido = new JComboBox();
		cmbNumeroElegido.setFont(new Font("Dialog", Font.PLAIN, 11));
		cmbNumeroElegido.setEditable(true);
		String strList[]= new String[99];
		for (int i=0;i<99;i++)
			strList [i]= Integer.toString(i+1);
		
		cmbNumeroElegido.setModel(new DefaultComboBoxModel(strList));
		
		cmbNumeroElegido.setBounds(215, 99, 90, 20);
		frame.getContentPane().add(cmbNumeroElegido);
	
	}
}
