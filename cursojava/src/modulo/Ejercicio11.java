package modulo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Ejercicio11 {

	private JFrame frame;
	private JTextField textField;
	private JLabel lblVocales;
	private JLabel lblContadorDeVocales;
	private JButton btnLimpiar;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio11 window = new Ejercicio11();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio11() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.GRAY);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(171, 71, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		
		JLabel lblIngreseUnaPalabra = new JLabel("Ingrese una palabra: ");
		lblIngreseUnaPalabra.setBounds(26, 74, 104, 14);
		frame.getContentPane().add(lblIngreseUnaPalabra);
		
		lblVocales = new JLabel("");
		lblVocales.setBackground(Color.GRAY);
		lblVocales.setOpaque(true);
		lblVocales.setBounds(26, 118, 398, 38);
		frame.getContentPane().add(lblVocales);
	
		JButton btnContar = new JButton("Contar ");
		btnContar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int contador = 0;
				String spalabra = String.valueOf(textField.getText());
				
				
					for(int x=0;x<spalabra.length();x++) {
						if ((spalabra.charAt(x)=='a') || (spalabra.charAt(x)=='e') || (spalabra.charAt(x)=='i') || (spalabra.charAt(x)=='o') || (spalabra.charAt(x)=='u')){ 
					    contador++;
					    
					    	lblVocales.setText("La palabra " + spalabra + " contiene " + contador + " vocales");
					  }
					}
			}
		});
		btnContar.setBounds(302, 194, 89, 23);
		frame.getContentPane().add(btnContar);
		
		lblContadorDeVocales = new JLabel("Contador de Vocales");
		lblContadorDeVocales.setFont(new Font("Dialog", Font.PLAIN, 25));
		lblContadorDeVocales.setBounds(90, 11, 236, 33);
		frame.getContentPane().add(lblContadorDeVocales);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				lblVocales.setText("");
				textField.setText(null);
			}
		});
		btnLimpiar.setBounds(72, 194, 89, 23);
		frame.getContentPane().add(btnLimpiar);
	}
}
