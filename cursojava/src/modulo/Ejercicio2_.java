package modulo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio2_ {

	private JFrame frame;
	private JTextField txtNum;
	private JLabel lblParoimpar;
	private JButton btnCalcular;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio2_ window = new Ejercicio2_();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio2_() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblParOImpar = new JLabel("Par o Impar");
		lblParOImpar.setFont(new Font("Dialog", Font.PLAIN, 18));
		lblParOImpar.setBounds(168, 11, 92, 24);
		frame.getContentPane().add(lblParOImpar);
		
		JLabel lblElijeUnNumero = new JLabel("Elije un numero");
		lblElijeUnNumero.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblElijeUnNumero.setBounds(74, 74, 98, 19);
		frame.getContentPane().add(lblElijeUnNumero);
		
		txtNum = new JTextField();
		txtNum.setBounds(195, 71, 86, 20);
		frame.getContentPane().add(txtNum);
		txtNum.setColumns(10);
		
		lblParoimpar = new JLabel("");
		lblParoimpar.setFont(new Font("Dialog", Font.BOLD, 18));
		lblParoimpar.setBackground(Color.GRAY);
		lblParoimpar.setBounds(168, 139, 117, 45);
		frame.getContentPane().add(lblParoimpar);
		
		btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int inum = Integer.parseInt(txtNum.getText());
				if (inum %2==0){
					lblParoimpar.setText("Es Par");
				}
				else {
					lblParoimpar.setText("Es Impar");
				}
				
				
			}
		});
		btnCalcular.setBounds(320, 70, 89, 23);
		frame.getContentPane().add(btnCalcular);
	}

}
