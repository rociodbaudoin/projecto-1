package modulo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio15 {

	
	private JFrame frame;
	private JLabel lblClas;
	private JComboBox cmbModelo;
	private Character Model;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio15 window = new Ejercicio15();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio15() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblEjercicio = new JLabel("Ejercicio 15");
		lblEjercicio.setFont(new Font("Dialog", Font.PLAIN, 24));
		lblEjercicio.setBounds(151, 11, 123, 32);
		frame.getContentPane().add(lblEjercicio);
		
		JLabel lblElijeUnModelo = new JLabel("Elije un modelo :");
		lblElijeUnModelo.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblElijeUnModelo.setBounds(10, 85, 104, 19);
		frame.getContentPane().add(lblElijeUnModelo);
		
		cmbModelo = new JComboBox();
		cmbModelo.setModel(new DefaultComboBoxModel(new String[] {"", "Chevrolet Classic LS", "Citro\u00EBn C3 Picasso 1.6 16v", "Fiat Uno Fire", "Fiat 147 ", "Ford Ka Fly", "Ford Fiesta One Edge Plus", "Peugeot Partner Patagonica 1.6 HDi VTC"}));
		cmbModelo.setBounds(124, 86, 80, 20);
		frame.getContentPane().add(cmbModelo);{
	
		
		JLabel lblClase = new JLabel("clase: ");
		lblClase.setFont(new Font("Dialog", Font.PLAIN, 15));
		lblClase.setBounds(10, 141, 43, 20);
		frame.getContentPane().add(lblClase);
		
		lblClas = new JLabel("");
		lblClas.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblClas.setBounds(69, 141, 104, 19);
		frame.getContentPane().add(lblClas);
		
		JLabel lblClaseA = new JLabel("Clase \"A\" los que tienen 4 ruedas y un motor.");
		lblClaseA.setBounds(10, 211, 791, 14);
		frame.getContentPane().add(lblClaseA);
		
		JLabel lblClaseB = new JLabel("Clase \"B\" 4 ruedas,  un motor, cierre centralizado y aire.");
		lblClaseB.setBounds(10, 224, 285, 14);
		frame.getContentPane().add(lblClaseB);
		
		JLabel lblClaseC = new JLabel("Clase \"C\" 4 ruedas,  un motor, cierre centralizado, aire, airbag.");
		lblClaseC.setBounds(10, 236, 303, 14);
		frame.getContentPane().add(lblClaseC);
		
		JButton btnVer = new JButton("Ver");
		btnVer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				char Model = Character.valueOf((char) cmbModelo.getSelectedItem());
				

			
			}
		});
		btnVer.setBounds(299, 85, 89, 23);
		frame.getContentPane().add(btnVer);
	}
}


	}

