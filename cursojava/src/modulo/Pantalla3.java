package modulo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla3 {

	private JFrame frame;
	private JTextField textnota1;
	private JTextField textnota2;
	private JTextField textnota3;
	private JLabel lblImagen;
	private JLabel lblResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla3 window = new Pantalla3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPromedioDeNotas = new JLabel("Promedio de Notas");
		lblPromedioDeNotas.setBounds(128, 11, 101, 14);
		frame.getContentPane().add(lblPromedioDeNotas);
		
		JLabel lblNota1 = new JLabel("Nota1");
		lblNota1.setBounds(10, 47, 46, 14);
		frame.getContentPane().add(lblNota1);
		
		textnota1 = new JTextField();
		textnota1.setBounds(92, 44, 86, 20);
		frame.getContentPane().add(textnota1);
		textnota1.setColumns(10);
		
		JLabel lblNota2 = new JLabel("Nota2");
		lblNota2.setBounds(10, 92, 46, 14);
		frame.getContentPane().add(lblNota2);
		

		textnota2 = new JTextField();
		textnota2.setBounds(92, 89, 86, 20);
		frame.getContentPane().add(textnota2);
		textnota2.setColumns(10);
		
		JLabel lblNota3 = new JLabel("Nota3");
		lblNota3.setBounds(10, 140, 46, 14);
		frame.getContentPane().add(lblNota3);
		
		textnota3 = new JTextField();
		textnota3.setBounds(92, 137, 86, 20);
		frame.getContentPane().add(textnota3);
		textnota3.setColumns(10);
		
		lblResultado = new JLabel("Resultado");
		lblResultado.setBounds(303, 92, 59, 14);
		frame.getContentPane().add(lblResultado);
		
		lblResultado = new JLabel("");
		lblResultado.setBackground(Color.PINK);
		lblResultado.setForeground(Color.GRAY);
		lblResultado.setOpaque(true);
		lblResultado.setBounds(288, 121, 86, 14);
		frame.getContentPane().add(lblResultado);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				float fnota1 = Float.parseFloat(textnota1.getText());
				float fnota2 = Float.parseFloat(textnota2.getText());
				float fnota3 = Float.parseFloat(textnota3.getText());
				
				float fpromedio = (fnota1+fnota2+fnota3)/3;
				
				lblResultado.setText(Float.toString(fpromedio));
				
				if (fpromedio >=7){
					lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoArriba_32px.png")));
				
				}
				else {
					lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoAbajo_32px.png")));
				}
				
				
				
				
				
				
				
				}
		});
		btnCalcular.setBounds(224, 215, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textnota1.setText("");
				textnota2.setText("");
				textnota3.setText("");
				
				
				
				lblResultado.setText("");
			}
		});
		btnLimpiar.setBounds(323, 215, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		lblImagen = new JLabel("");
		lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoArriba_32px.png")));
		lblImagen.setBounds(308, 149, 32, 39);
		frame.getContentPane().add(lblImagen);
		
	
		
		
		
	}
}
