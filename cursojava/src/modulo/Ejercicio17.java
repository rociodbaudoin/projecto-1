package modulo;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.AbstractButton;
import javax.swing.AbstractListModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class Ejercicio17 {

	private JFrame frame;
	private String values[]=new String[10];
	private JComboBox cmbTabla;
	private JList list;
	private JLabel lblFila;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio17 window = new Ejercicio17();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio17() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.GRAY);
		frame.getContentPane().setFont(new Font("Dialog", Font.PLAIN, 11));
		frame.setBounds(100, 100, 543, 511);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Tablas");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel.setBounds(206, 33, 109, 39);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblElijaTabla = new JLabel("Elija tabla");
		lblElijaTabla.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		lblElijaTabla.setBounds(45, 110, 91, 21);
		frame.getContentPane().add(lblElijaTabla);
		
		String strListTablas[] = new String [10];
		for(int i=0;i<10;i++){
			strListTablas[i] = Integer.toString(i+1);
		}
		cmbTabla = new JComboBox();
		String  strTablas[] = new String[10]; 
		for(int i=0;i<10;i++)
			strTablas[i]=Integer.toString(i+1);
			
		
		cmbTabla.setModel(new DefaultComboBoxModel(strTablas));
		
		cmbTabla.setBounds(165, 113, 109, 20);
		frame.getContentPane().add(cmbTabla);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.setBorder(UIManager.getBorder("CheckBox.border"));
		btnCalcular.setBackground(Color.GRAY);
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				int iTabla=Integer.parseInt((String) cmbTabla.getSelectedItem());
				int prod=0;
				if(prod %2==0){
				for(int i=0;i<10;i++){
					prod=iTabla*i;
					values[i]= iTabla  + "x" + i + "=" + prod;
					
						
				}
						
				}
				
				list.setModel(new AbstractListModel() {
					
					public int getSize() {
						return values.length;
					}
					public Object getElementAt(int index) {
						return values[index];
					}
				});
			
		};
		});
		btnCalcular.setBounds(341, 112, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(145, 186, 109, 191);
		frame.getContentPane().add(scrollPane);
		
		list = new JList();
		list.setBackground(Color.GRAY);
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				lblFila.setText(values[list.getSelectedIndex()]);
				
			}
		});
		
		list.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));

		scrollPane.setViewportView(list);
		
		lblFila = new JLabel("");
		lblFila.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 24));
		lblFila.setBackground(Color.GRAY);
		lblFila.setOpaque(true);
		lblFila.setBounds(375, 227, 100, 39);
		frame.getContentPane().add(lblFila);
		
	}
}
 

