package modulo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio7 {

	private JFrame frame;
	private JTextField txtVar1;
	private JTextField txtVar2;
	private JTextField txtVar3;
	private JLabel lblMayor;
	private JButton btnMostrar;
	private JButton btnLimpiar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio7 window = new Ejercicio7();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio7() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNumeroMayor = new JLabel("Numero Mayor");
		lblNumeroMayor.setFont(new Font("Dialog", Font.BOLD, 18));
		lblNumeroMayor.setBounds(140, 11, 129, 24);
		frame.getContentPane().add(lblNumeroMayor);
		
		txtVar1 = new JTextField();
		txtVar1.setBounds(44, 60, 86, 20);
		frame.getContentPane().add(txtVar1);
		txtVar1.setColumns(10);
		
		txtVar2 = new JTextField();
		txtVar2.setBounds(174, 60, 86, 20);
		frame.getContentPane().add(txtVar2);
		txtVar2.setColumns(10);
		
		txtVar3 = new JTextField();
		txtVar3.setBounds(304, 60, 86, 20);
		frame.getContentPane().add(txtVar3);
		txtVar3.setColumns(10);
		
		JLabel lblElMayorEs = new JLabel("El mayor es:");
		lblElMayorEs.setFont(new Font("Dialog", Font.BOLD, 15));
		lblElMayorEs.setBounds(88, 123, 85, 20);
		frame.getContentPane().add(lblElMayorEs);
		
		lblMayor = new JLabel("");
		lblMayor.setFont(new Font("Dialog", Font.BOLD, 18));
		lblMayor.setOpaque(true);
		lblMayor.setBackground(Color.GRAY);
		lblMayor.setBounds(261, 123, 85, 20);
		frame.getContentPane().add(lblMayor);
		
		btnMostrar = new JButton("Mostrar");
		btnMostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int ivar1 = Integer.parseInt(txtVar1.getText());
				int ivar2 = Integer.parseInt(txtVar2.getText());
				int ivar3 = Integer.parseInt(txtVar3.getText());
				
				if (ivar1>ivar2){
					lblMayor.setText(txtVar1.getText());}
				
				if (ivar2>ivar1){
					lblMayor.setText(txtVar2.getText());}
				
				if (ivar3>ivar1){
					lblMayor.setText(txtVar3.getText());}
				
				if (ivar2>ivar3){
					lblMayor.setText(txtVar2.getText());}
				
				if (ivar1>ivar3){
					lblMayor.setText(txtVar1.getText());}
				
				
	
		
			    }
			
		});
		btnMostrar.setBounds(275, 188, 89, 23);
		frame.getContentPane().add(btnMostrar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtVar1.setText("");
				txtVar2.setText("");
				txtVar3.setText("");
				lblMayor.setText("");
			}
			
		});
		btnLimpiar.setBounds(101, 188, 89, 23);
		frame.getContentPane().add(btnLimpiar);
	}

}
