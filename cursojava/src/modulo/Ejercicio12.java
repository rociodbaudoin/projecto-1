package modulo;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio12 {

	private JFrame frame;
	private JTextField txtNum;
	private JLabel lblDoc;
	private JButton btnVer;
	private JButton btnLimpiar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio12 window = new Ejercicio12();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio12() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Dialog", Font.PLAIN, 11));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblEjercicio = new JLabel("Ejercicio12");
		lblEjercicio.setFont(new Font("Dialog", Font.PLAIN, 24));
		lblEjercicio.setBounds(144, 11, 116, 32);
		frame.getContentPane().add(lblEjercicio);
		
		JLabel lblIngreseUnNumero = new JLabel("Ingrese un numero: ");
		lblIngreseUnNumero.setBounds(26, 93, 98, 14);
		frame.getContentPane().add(lblIngreseUnNumero);
		
		txtNum = new JTextField();
		txtNum.setBounds(144, 90, 86, 20);
		frame.getContentPane().add(txtNum);
		txtNum.setColumns(10);
		
		lblDoc = new JLabel("");
		lblDoc.setFont(new Font("Dialog", Font.PLAIN, 14));
		lblDoc.setBounds(26, 144, 398, 32);
		frame.getContentPane().add(lblDoc);
		
		btnVer = new JButton("Ver ");
		btnVer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int inum = Integer.parseInt((String)txtNum.getText());
			     	
			  if (inum<12 || inum==12){ 
				  lblDoc.setText( " Pertenece a la primera docena.");}
				    
			  	if (inum>13) { 
				  lblDoc.setText(" Pertenece a la segunda docena."); } 
			     	
					if (inum>24) {
						lblDoc.setText(" Pertenece a la tercera docena.");}

						if (inum>36)  {
			     		lblDoc.setText(" El numero no se encuentra en el rango.");  }
			     	 
			     	 
				 }
			
			
		
			
		});
		btnVer.setBounds(273, 211, 89, 23);
		frame.getContentPane().add(btnVer);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 lblDoc.setText("");
				
			}
		});
		btnLimpiar.setBounds(84, 211, 89, 23);
		frame.getContentPane().add(btnLimpiar);
			}
}
	

