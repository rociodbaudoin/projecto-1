package modulo;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractListModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;

public class Ejercicio18 {

	protected static final String String= null;
	private JFrame frame;
	private JScrollPane scrollPane;
	private String[] strTitulos;
	private String[][] strTablas;
	private JComboBox cmbDesde;
	private JComboBox cmbHasta;
	private JTable table;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio18 window = new Ejercicio18();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio18() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.GRAY);
		frame.getContentPane().setFont(new Font("Dialog", Font.PLAIN, 11));
		frame.setBounds(100, 100, 794, 511);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Tablas");
		lblNewLabel.setBounds(288, 0, 109, 39);
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.setBounds(62, 16, 89, 23);
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int iTablaDesde=Integer.parseInt((String)cmbDesde.getSelectedItem());
				int iTablaHasta=Integer.parseInt((String)cmbHasta.getSelectedItem());
				int iCantColum=iTablaHasta -iTablaDesde+1;
				strTitulos = new String [iCantColum];
				strTablas = new String [10][iCantColum];
				
				for(int col=0;col<iCantColum;col++){
					strTitulos[col]=("tabla del " + (iTablaDesde + col));
				for(int fila=0;fila<10;fila++)
					strTablas[fila][col]=(iTablaDesde + col) + "x" + fila + "="+ (fila*(iTablaDesde + col));
			}
				
				
				table.setModel(new DefaultTableModel(
						strTablas,
						strTitulos
					));
			}
			
		});
		
		table = new JTable();
		table.setBounds(51, 73, 504, 242);
		
		frame.getContentPane().add(table);
		frame.getContentPane().add(btnCalcular);
		
		cmbDesde = new JComboBox();
		cmbDesde.setBounds(62, 386, 157, 20);
		cmbDesde.setModel(new DefaultComboBoxModel(new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		frame.getContentPane().add(cmbDesde);
		
		cmbHasta = new JComboBox();
		cmbHasta.setBounds(350, 386, 142, 20);
		cmbHasta.setModel(new DefaultComboBoxModel(new String[] {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		frame.getContentPane().add(cmbHasta);
	
	}
	public JTable getTable() {
		return table;
	}
}
	
		
		